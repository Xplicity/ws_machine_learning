import cv2
import gym
import numpy as np

from Agent import Agent

GENERATIONS = 1000


class Environment:
    def __init__(self, population_size):
        self.population_size = population_size
        self.population = self.build_population()
        self.generation = 0

    def build_population(self, based_on: Agent = None):
        return [self.spawn_agent(based_on) for
                _ in range(self.population_size)]

    def spawn_agent(self, parent: Agent = None):
        if parent is None:
            return Agent(6)
        return parent.spawn()

    def run(self):
        for _ in range(GENERATIONS):
            for agent in self.population:
                self.trial(agent)
            self.population = self.build_population(
                self.get_fittest())

    def trial(self, agent: Agent):
        env = gym.make('SpaceInvaders-v0')
        env.reset()

        observation, reward, done, info = env.step(0)
        normalized = self.normalize(observation)
        agent.set_initial_perception(normalized)

        done = False
        while not done:
            action = agent.get_action()
            observation, reward, done, info = env.step(action)
            normalized = self.normalize(observation)
            agent.update_perception(
                normalized, reward, done, action)

            env.render()

        env.close()

    def normalize(self, observation):
        observation = cv2.cvtColor(
            cv2.resize(observation, (84, 110)),
            cv2.COLOR_BGR2GRAY)
        observation = observation[26:110, :]
        _, observation = cv2.threshold(
            observation, 1, 255, cv2.THRESH_BINARY)
        return np.reshape(observation, (84, 84, 1))

    def get_fittest(self):
        fittest = self.population[0]
        for agent in self.population:
            if agent.get_fitness() > fittest.get_fitness():
                fittest = agent
        return fittest

    def __str__(self) -> str:
        return str([str(a) for a in self.population])
