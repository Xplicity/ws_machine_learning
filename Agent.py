import numpy as np

from Brain import Brain
from Brain import OBSERVATION_BEFORE_TRAINING


class Agent:

    def __init__(self, actions):
        self.current_state = None
        self.fitness = 0
        self.time = 0

        self.brain = Brain(actions)

    def set_initial_perception(self, observation):
        self.current_state = np.squeeze(np.stack(
            (observation, observation, observation, observation),
            axis=2))

    def update_perception(self, observation, reward, done, action):
        new_state = np.append(
            observation,
            self.current_state[:, :, 1:],
            axis=2)

        self.brain.memorize(
            self.current_state, new_state,
            reward, done, action)

        if self.time > OBSERVATION_BEFORE_TRAINING:
            self.brain.tain()

        self.current_state = new_state
        self.fitness += reward
        self.time += 1

    def get_fitness(self):
        return self.fitness

    def get_action(self):
        return np.argmax(self.brain.get_action(
            self.current_state))

    def spawn(self):
        agent = Agent(self.brain.actions)
        agent.brain = self.brain
        return agent

    def __str__(self) -> str:
        return "agent"
