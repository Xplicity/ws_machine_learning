import random
from collections import deque

import numpy as np
import tensorflow as tf

BATCH_SIZE = 32
FINAL_EPSILON = 0.1
INITIAL_EPSILON = 1.0
MEMORY_CAPACITY = 10000
OBSERVATION_BEFORE_TRAINING = 15000


class Brain:
    def __init__(self, actions) -> None:
        self.memory = deque()
        self.epsilon = INITIAL_EPSILON

        self.actions = actions
        self.state_input, self.q = self.dqn()
        self.state_input_target, self.q_target = self.dqn()

        self.session = tf.InteractiveSession()
        self.session.run(
            tf.global_variables_initializer())

    def memorize(self, current_state, new_state, reward, done, action):
        self.memory.append((current_state, new_state, reward, done, action))
        if len(self.memory) > MEMORY_CAPACITY:
            self.memory.popleft()

    def get_action(self, current_state):
        q = self.q.eval(feed_dict={
            self.state_input: [current_state]
        })[0]

        action = np.zeros(self.actions)
        if random.random() <= self.actions:
            action_index = random.randrange(self.actions)
            action[action_index] = 1
        else:
            action_index = np.argmax(q)
            action[action_index] = 1

        if self.epsilon > FINAL_EPSILON:
            self.epsilon -= (INITIAL_EPSILON - FINAL_EPSILON)

        return action

    def dqn(self):
        state_input = tf.placeholder(
            "float", [None, 84, 84, 4])

        hidden1 = tf.nn.relu(self._conv2d(
            state_input,
            self._weighted_variable([8, 8, 4, 32]),
            4))

        hidden2 = tf.nn.relu(self._conv2d(
            hidden1,
            self._weighted_variable([4, 4, 32, 64]),
            2))

        hidden3 = tf.nn.relu(self._conv2d(
            hidden2,
            self._weighted_variable([3, 3, 64, 64]),
            1))

        flat = tf.reshape(hidden3, [-1, 3136])
        final = tf.nn.relu(tf.matmul(
            flat,
            self._weighted_variable([3136, 512])))

        q = tf.matmul(final, self._weighted_variable(
            [512, self.actions]))

        return state_input, q

    def tain(self):
        batch = random.sample(self.memory, BATCH_SIZE)
        # state_bstch = mem

    def _conv2d(self, x, W, stride):
        return tf.nn.conv2d(x, W, strides=[1, stride, stride, 1], padding="VALID")

    def _weighted_variable(self, shape):
        return tf.Variable(tf.truncated_normal(shape, stddev=0.01))
